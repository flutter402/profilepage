import 'package:flutter/material.dart';

void main() {
  runApp(ContactProfilePage());
}

enum APP_THEME{LIHGT,DARK}

class MyAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(
    brightness: Brightness.light,
    appBarTheme: AppBarTheme(
    color: Colors.white,
    iconTheme: IconThemeData(
      color: Colors.indigo.shade500,
    ),
    ),
    );
  }

  static ThemeData appThemeDark() {
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Colors.black,
        iconTheme: IconThemeData(
          color: Colors.indigo.shade500,
        ),
      ),
    );
  }
}

class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIHGT;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK
          ? MyAppTheme.appThemeLight()
          : MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.sunny),
          onPressed: () {
            setState(() {

              currentTheme == APP_THEME.DARK
              ? currentTheme = APP_THEME.LIHGT
              : currentTheme = APP_THEME.DARK;
            });
          },
        ),
      ),
    );
  }
}

AppBar buildAppBarWidget(){
  return AppBar(
    backgroundColor: Colors.lightGreenAccent,
    leading: Icon(
      Icons.arrow_back,
      color: Colors.black,
    ),
    actions: <Widget>[
      IconButton(onPressed: () {},
        icon: Icon(Icons.star_border),
        color: Colors.black,
      )
    ],

  );
}

Widget buildBodyWidget() {
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(
            width: double.infinity,

            //Height constraint at Container widget level
            height: 250,

            child: Image.network(
              "https://i.pinimg.com/originals/65/b3/02/65b3027496af9eb38beae33e0f1ba670.jpg",
              fit: BoxFit.cover,
            ),
          ),
          Container(
              height: 60,
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text("Suraida Binyoh",
                      style: TextStyle(fontSize: 30),
                    ),
                  )
                ],
              )
          ),
          Divider(
            color: Colors.grey,
          ),

          Container(
            margin: EdgeInsets.only(top: 8, bottom: 8),
            child: Theme(
              data: ThemeData (
                  iconTheme: IconThemeData(
                    color: Colors.teal,
                  )
              ),
              child: profileActionItems(),
            ),
          ),

          mobilePhoneListTile(),
          otherPhoneListTile(),
          Divider(
            color: Colors.grey,
          ),

          emailListTile(),
          Divider(
            color: Colors.grey,
          ),

          addressListTile(),
        ],
      ),
    ],
  );
}

Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
        //  color: Colors.teal,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message,
        //  color: Colors.teal,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}

Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
         // color: Colors.teal,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}
Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
         // color: Colors.teal,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}
Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
         // color: Colors.teal,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}
Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
        //  color: Colors.teal,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}

//ListTile
Widget mobilePhoneListTile(){
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("063-614-2752"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon: Icon(Icons.message),
     // color: Colors.teal,
      onPressed: () {},
    ),
  );
}

Widget otherPhoneListTile(){
  return ListTile(
    leading: Text(""),
    title: Text("440-440-3390"),
    subtitle: Text("other"),
    trailing: IconButton(
      icon: Icon(Icons.message),
     // color: Colors.teal,
      onPressed: () {},
    ),
  );
}

Widget emailListTile(){
  return ListTile(
    leading: Icon(Icons.email),
    title: Text("63160192@go.buu.ac.th"),
    subtitle: Text("work"),
  );
}

Widget addressListTile(){
  return ListTile(
    leading: Icon(Icons.location_on),
    title: Text("43/1 Mueng, Yala"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon(Icons.directions),
    //  color: Colors.teal,
      onPressed: () {},
    ),
  );
}


Widget profileActionItems() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoCallButton(),
      buildEmailButton(),
      buildDirectionsButton(),
      buildPayButton()
    ],
  );
}